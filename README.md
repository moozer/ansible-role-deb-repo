Deb-repo
=========

This role creates an apt repository with signed releases.

Uses scripts from [trickv](https://github.com/trickv/zoom-ubuntu-repo)

https://debian-handbook.info/browse/stable/sect.setup-apt-package-repository.html
https://www.linuxbabe.com/security/a-practical-guide-to-gpg-part-1-generate-your-keypair
https://wiki.debian.org/DebianRepository/UseThirdParty


1. set up a repo in /srv/packages using mini-dinstall
2. make nginx serve it
3. set up gpg keys
4. add gpg key to client
4. add deb http://.../updates to sources.list on client




Requirements
------------

No special requirements

Role Variables
--------------

All variables used are specified in [defaults/main.yml](defaults/main.yml).


Dependencies
------------

No dependencies

Example Playbook
----------------

See [tests/test.yml] for an example.

Minium survival is something like this when using sudo

```
- hosts: debrepo
  become: true

  vars:
    dep_repo_user_name: "repo master"
    deb_repo_mail: "repo_master@mydomain"
    deb_repo_name: "localcache"
    deb_repo_release_origin: repo.mydomain
    deb_repo_description: "Local mirror for non-debian repo packages"
    deb_repo_label: remote
    deb_repo_component: main

  roles:
    - deb_repo
```

see [the debian wiki](https://wiki.debian.org/DebianRepository/Format#A.22Release.22_files) for details on lables, components, etc.


To add more fetch scripts to the system, do the following:

1. Create a download script for the program.

    Look at [fetch_zoom.sh.j2](templates/fetch_zoom.sh.j2) for inspiration

2. Add `{{ deb_repo_scripts_dir }}/update_repo.sh <package>` to add it to the repo.

    I add the scripts to `{{ deb_repo_scripts_dir }}` (which is owned by root) to avoid tampering, but it could be anywhere that `{{deb_repo_user}}` may execute from.

3. Add a cron job to periodicly update the package.

    Ensure that the cron job is owned by `{{deb_repo_user}}`. This is needed for the gpg key signing.

License
-------

BSD

Author Information
------------------

Master repo is on [gitlab](https://gitlab.com/moozer/ansible-role-deb-repo)
